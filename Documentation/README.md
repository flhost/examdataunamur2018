# README #

### Explanation of the structure ###

There are four repositories : Code, Data, Documentation and Results

In the first one, you can find our codes.
In Exploratory, you can find the types and the amount of food and drink which are sold in the coffeebar. There is
also the number of unique customer (number of customers which are only came once). Finally, there are four dataframes :
for both food and drinks, there is a dataframe with probabilities, and one with cumulative probabilities.
In Definition, you can find all the definitions which are needed for the simulation.
In Simulation, we simulate the 5 year span.
In part 4, we calculate the number of returning customers and the probabilities of having a onetime of a returning customer at a given time.
There are the changes of the codes if the returning customers drop to 50, if the prices go up by 20% from the beginning of 2015
and if the budget of hipsters drop to 40.


In the repository Data, there is the dataset Coffeebar.

In the repository Documentation, there are this file and the information for the exam.

In Result, at the end of the execution of the simulation, you'll find :
- the Plots of amount of Food and Drinks sold (part 1)
- the Plots of amount of Food and Drinks sold (Part 3)
- the Plots of average income per hour and per month (Part 3)
- the Dataframes of cumulative probabilities for drinks and food (Part 1)
- the Final Dataframe of simulation (Part 3)
- An empty python file

### Who do I talk to? ###

* This work is made by Olivia and Faustine
