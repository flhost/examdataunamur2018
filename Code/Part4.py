# Part 4
import pandas as pd
import matplotlib.pyplot as plt
path = "./Data/"
path2 = "./Results/"

Coffeebar = pd.read_csv(path+"Coffeebar_2013-2017.csv", sep=";")

# Number of returning customers, a probability of having a onetime or returning customer at a given time
Customer_count = Coffeebar['CUSTOMER'].value_counts()
print(len(Customer_count[Customer_count>1])) #number of returning customers

Data_Fr = Coffeebar[['CUSTOMER']]
Data_Fr['COUNT'] = 1
New_D = Data_Fr[['COUNT', 'CUSTOMER']].groupby('CUSTOMER', as_index = False).sum()
New_D = New_D.sort_values('COUNT')
New_D_OneTime = New_D[: -1000]
New_D_Ret = New_D[-1000:]

RetList = New_D_Ret['CUSTOMER'].tolist()
OneTimeList = New_D_OneTime['CUSTOMER'].tolist()

def TypeFct(x):
    if x in RetList:
        return 'Returning'
    else:
        return 'One Time'

Coffeebar['Type'] = Coffeebar['CUSTOMER'].map(TypeFct)

def RetFct(x):
    if x in RetList:
        return 1/1825*100
    else:
        return 0

Coffeebar['RETURNING'] = Coffeebar['CUSTOMER'].map(RetFct)

def OneTimeFct(x):
    if x not in RetList:
        return 1/1825*100
    else:
        return 0

Coffeebar['OneTime'] = Coffeebar['CUSTOMER'].map(OneTimeFct)

Coffeebar['HOUR'] = Coffeebar['TIME'].str[11:16]

Prob_Customer = Coffeebar[['HOUR','OneTime','RETURNING']].groupby('HOUR', as_index=False).sum() #probabilities

# plots to see correlation

Coffeebar['YEAR'] = Coffeebar['TIME'].str[0:4]

#Bar plot for food
def Sandfct(x):
    if x == 'sandwich':
        return 1
    else:
        return 0

Coffeebar['Sandwich'] = Coffeebar['FOOD'].map(Sandfct)

def Piefct(x):
    if x == 'pie':
        return 1
    else:
        return 0

Coffeebar['Pie'] = Coffeebar['FOOD'].map(Piefct)

def Muffinfct(x):
    if x == 'muffin':
        return 1
    else:
        return 0

Coffeebar['Muffin'] = Coffeebar['FOOD'].map(Muffinfct)

def Cookiefct(x):
    if x == 'cookie':
        return 1
    else:
        return 0

Coffeebar['Cookie'] = Coffeebar['FOOD'].map(Cookiefct)

def Nothingfct(x):
    if x not in ('sandwich', 'pie', 'muffin', 'cookie'):
        return 1
    else:
        return 0

Coffeebar['Nothing'] = Coffeebar['FOOD'].map(Nothingfct)

Food_Type = Coffeebar[['Type','Sandwich','Pie', 'Cookie', 'Muffin', 'Nothing']].groupby('Type', as_index=False).sum()
Food_Type.plot(x='Type', y=['Sandwich', 'Pie', 'Cookie', 'Muffin', 'Nothing'], kind="bar", title='Number of foods bought by customers')
plt.savefig(path2 + "Food_Type") #do the two commands at th same time

#plot for drinks
def Frappfct(x):
    if x == 'frappucino':
        return 1
    else:
        return 0

Coffeebar['Frappucino'] = Coffeebar['DRINKS'].map(Frappfct)

def Sodafct(x):
    if x == 'soda':
        return 1
    else:
        return 0

Coffeebar['Soda'] = Coffeebar['DRINKS'].map(Sodafct)

def Coffeefct(x):
    if x == 'coffee':
        return 1
    else:
        return 0

Coffeebar['Coffee'] = Coffeebar['DRINKS'].map(Coffeefct)

def Teafct(x):
    if x == 'tea':
        return 1
    else:
        return 0

Coffeebar['Tea'] = Coffeebar['DRINKS'].map(Teafct)

def Waterfct(x):
    if x == 'water':
        return 1
    else:
        return 0

Coffeebar['Water'] = Coffeebar['DRINKS'].map(Waterfct)

def Milkfct(x):
    if x =='milkshake':
        return 1
    else: return 0

Coffeebar['Milkshake'] = Coffeebar['DRINKS'].map(Milkfct)

Drinks_Type = Coffeebar[['Type','Frappucino','Tea', 'Water', 'Coffee','Soda', 'Milkshake']].groupby('Type', as_index=False).sum()

Drinks_Type.plot(x='YEAR', y=['Frappucino', 'Tea', 'Water', 'Coffee','Soda', 'Milkshake'], kind="bar", title='Number of drinks bought per customers')
plt.savefig(path2 + "Drinks_Type") #do the two commands at th same time

#What if...
#50 returning customer

    #Lignes 33 - 49

CustomerReturning_List = []

for i in range(50):
    ID = i
    if i <= 17:
        hipster_flag = True
        budget = 500
    else:
        hipster_flag = False
        budget = 250
    CustomerReturning_List.append(Returning(hipster_flag, budget, ID))

CustomerOneTime_List = []

ID_precedant = 100000
t = 50
budget_max = 10


#Prices change
    # lignes 52 - 56
New_DataFrame['YEAR'] = New_DataFrame['TIME'].str[0:4]

for (index, row) in New_DataFrame.iterrows():
    tips = 0
    New_DataFrame.loc[index, 'FOOD'] = Give_Food(index)
    New_DataFrame.loc[index, 'DRINK'] = Give_Drink(index)
    if int(New_DataFrame.loc[index,'YEAR']) <= 2014:
        New_DataFrame.loc[index, 'FOOD_P'] = PriceF(index, New_DataFrame, Data_Price)
        New_DataFrame.loc[index, 'DRINK_P'] = PriceD(index, New_DataFrame, Data_Price)
    else:
        New_DataFrame.loc[index, 'FOOD_P'] = PriceF(index, New_DataFrame, Data_Price) * 1.2
        New_DataFrame.loc[index, 'DRINK_P'] = PriceD(index, New_DataFrame, Data_Price) * 1.2



#budget of hipster drops to 40
    #lignes 32 - 42
CustomerReturning_List = []

for i in range(1000):
    ID = i
    if i <= 333:
        hipster_flag = True
        budget = 500
    else:
        hipster_flag = False
        budget = 40
    CustomerReturning_List.append(Returning(hipster_flag, budget, ID))

#Our Question : What if the proportion changes : 70% of Returning and 30% of one time
    #ligne 52

if i > 70: