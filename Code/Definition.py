# Part2

import pandas as pd
path2 = "./Results/"
from random import randint

ProbDrinkPart3 = pd.read_csv(path2 + 'ProbDrinkPart3', index_col=0)
ProbFoodPart3 = pd.read_csv(path2 + 'ProbFoodPart3', index_col=0)

# Definition of classes

class Customer (object):
    def __init__(self, ID):
        self.ID = ID
    def GetHistory(self, New_DataFrame):
        print(New_DataFrame.loc[New_DataFrame.CUSTOMER == self.ID])

class OneTime (Customer):
    def __init__(self, tripadvisor_flag, budget, *args, **kwargs):
        Customer.__init__(self, *args, **kwargs)
        self.tripadvisor_flag = tripadvisor_flag
        self.budget = 100
    def Bonus(self):
        if OneTime.tripadvisor_flag:
            tips = randint(1, 10)
        else :
            tips = 0
        return tips

class Returning (Customer):
    def __init__(self, hipster_flag,budget, *args, **kwargs):
        Customer.__init__(self, *args, **kwargs)
        self.hipster_flag = hipster_flag
        self.budget = budget


#Definition of functions

def Give_Food(index):
    f = (randint(1, 10000)) / 100
    if f <= ProbFoodPart3.loc[index[11:16], 'Sandwich']:
        return 'Sandwich'
    elif f <= ProbFoodPart3.loc[index[11:16], 'Pie']:
        return 'Pie'
    elif f <= ProbFoodPart3.loc[index[11:16], 'Cookie']:
        return 'Cookie'
    elif f <= ProbFoodPart3.loc[index[11:16], 'Muffin']:
        return 'Muffin'
    else:
        return 'Nothing'

def Give_Drink(index):
    d = (randint(1, 10000)) / 100
    if d <= ProbDrinkPart3.loc[index[11:16], 'Frappucino']:
            return 'Frappucino'
    elif d <= ProbDrinkPart3.loc[index[11:16], 'Soda']:
            return 'Soda'
    elif d <= ProbDrinkPart3.loc[index[11:16], 'Coffee']:
           return 'Coffee'
    elif d <= ProbDrinkPart3.loc[index[11:16], 'Tea']:
            return 'Tea'
    elif d <= ProbDrinkPart3.loc[index[11:16], 'Water']:
            return 'Water'
    else:
            return 'Milkshake'

def PriceF(index, New_DataFrame, Data_Price):           #Give Price of food
    if New_DataFrame.loc[index, 'FOOD'] == 'Sandwich':
        return Data_Price.loc['Sandwich', 'Price']
    elif New_DataFrame.loc[index, 'FOOD'] == 'Pie':
        return Data_Price.loc['Pie', 'Price']
    elif New_DataFrame.loc[index, 'FOOD'] == 'Cookie':
        return Data_Price.loc['Cookie', 'Price']
    elif New_DataFrame.loc[index, 'FOOD'] == 'Muffin':
        return Data_Price.loc['Muffin', 'Price']
    elif New_DataFrame.loc[index, 'FOOD'] == 'Nothing':
        return Data_Price.loc['Nothing', 'Price']

def PriceD(index, New_DataFrame, Data_Price):           #Give Price of Drinks
    if New_DataFrame.loc[index, 'DRINK'] == 'Water':
        return Data_Price.loc['Water', 'Price']
    elif New_DataFrame.loc[index, 'DRINK'] == 'Tea':
        return Data_Price.loc['Tea', 'Price']
    elif New_DataFrame.loc[index, 'DRINK'] == 'Coffee':
        return Data_Price.loc['Coffee', 'Price']
    elif New_DataFrame.loc[index, 'DRINK'] == 'Frappucino':
        return Data_Price.loc['Frappucino', 'Price']
    elif New_DataFrame.loc[index, 'DRINK'] == 'Milkshake':
        return Data_Price.loc['Milkshake', 'Price']
    elif New_DataFrame.loc[index, 'DRINK'] == 'Soda':
        return Data_Price.loc['Soda', 'Price']





