#Part 3
import random
from random import randint
import pandas as pd

path = "./Data/"
path2 = "./Results/"
ProbDrinkPart3 = pd.read_csv(path2+ 'ProbDrinkPart3', index_col=0)
ProbFoodPart3 = pd.read_csv(path2 + 'ProbFoodPart3', index_col=0)
Coffeebar = pd.read_csv(path + "Coffeebar_2013-2017.csv", sep=";")
from Code.Definition import Customer, OneTime, Returning, Give_Food, Give_Drink, PriceF, PriceD

# Creation of a new DataFrame which is empty and will be completed during the simulation

New_DataFrame = Coffeebar[['TIME']]
New_DataFrame['CUSTOMER']= ""
New_DataFrame['DRINK'] = ""
New_DataFrame['DRINK_P'] = ""
New_DataFrame['FOOD'] = ""
New_DataFrame['FOOD_P'] = ""
New_DataFrame['TIPS'] = ""
New_DataFrame['TOTAL'] = ""
New_DataFrame = New_DataFrame.set_index("TIME")

# Creation of the DataFrame with Prices of Food and Drinks (used in part 2)

Data_Price = pd.DataFrame({'Price':[5, 3, 3, 2, 0, 2, 5, 3, 3, 3, 4]}, index = ['Sandwich', 'Pie', 'Muffin', 'Cookie', 'Nothing', 'Water', 'Milkshake', 'Tea', 'Soda', 'Coffee', 'Frappucino'])

# Creation of the two lists of Customers (returning and one time)

CustomerReturning_List = []

for i in range(1000):
    ID = i
    if i <= 333:
        hipster_flag = True
        budget = 500
    else:
        hipster_flag = False
        budget = 250
    CustomerReturning_List.append(Returning(hipster_flag, budget, ID))

CustomerOneTime_List = []

# Function which fills the Data Frame row by row (!! It Takes 30 mins, if you don't want to wait, you can open the file 'New_DataFame')

ID_precedant = 100000
t = 1000
budget_max = 10

for (index, row) in New_DataFrame.iterrows():
    New_DataFrame.loc[index, 'FOOD'] = Give_Food(index)
    New_DataFrame.loc[index, 'DRINK'] = Give_Drink(index)
    New_DataFrame.loc[index, 'FOOD_P'] = PriceF(index, New_DataFrame, Data_Price)
    New_DataFrame.loc[index, 'DRINK_P'] = PriceD(index, New_DataFrame, Data_Price)
    tips = 0
    if CustomerReturning_List[0].budget > budget_max:
        i = randint(1, 100)
        if i > 20:                                  # OneTime
            z = randint(1, 100)
            if z < 10:                                  # Tripadvisor
                tripadvisor_flag = True
                tips = randint(1, 10)
            else:                                       # Not tripadvisor
                tripadvisor_flag = False
            ID_precedant = ID_precedant + 1
            ID = ID_precedant
            New_DataFrame.loc[index, 'CUSTOMER'] = ID
            budget = 100
            CustomerOneTime_List.append(OneTime(tripadvisor_flag, ID, budget))
        else:                                       # returning
            y = randint(1, t)
            k = y-1
            New_DataFrame.loc[index, 'CUSTOMER'] = CustomerReturning_List[k].ID
            CustomerReturning_List[k].budget = CustomerReturning_List[k].budget - New_DataFrame.loc[index, 'DRINK_P'] - New_DataFrame.loc[index, 'FOOD_P']
            if CustomerReturning_List[k].budget < budget_max:
                CustomerReturning_List[k],CustomerReturning_List[t-1] = CustomerReturning_List[t-1], CustomerReturning_List[k]
                t = t - 1
    else:
        z = randint(1, 100)
        if z < 10:  # Tripadvisor
            tripadvisor_flag = True
            tips = randint(1, 10)
        else:  # Not tripadvisor
            tripadvisor_flag = False
        ID_precedant = ID_precedant + 1
        ID = ID_precedant
        New_DataFrame.loc[index, 'CUSTOMER'] = ID
        budget = 100
        CustomerOneTime_List.append(OneTime(tripadvisor_flag, ID, budget))
    New_DataFrame.loc[index, 'TIPS'] = int(tips)
    New_DataFrame.loc[index, 'TOTAL'] = New_DataFrame.loc[index, 'DRINK_P'] + New_DataFrame.loc[index, 'FOOD_P'] + New_DataFrame.loc[index, 'TIPS']


New_DataFrame.to_csv(path2 + 'New_DataFrame', sep=',')

#plots
import matplotlib.pyplot as plt
New_DataFrame = pd.read_csv(path2 + 'New_DataFrame')
New_DataFrame['YEAR'] = New_DataFrame['TIME'].str[0:4]

# Bar plot for food
def Sandfct(x):
    if x == 'Sandwich':
        return 1
    else:
        return 0

New_DataFrame['Sandwich'] = New_DataFrame['FOOD'].map(Sandfct)

def Piefct(x):
    if x == 'Pie':
        return 1
    else:
        return 0

New_DataFrame['Pie'] = New_DataFrame['FOOD'].map(Piefct)

def Muffinfct(x):
    if x == 'Muffin':
        return 1
    else:
        return 0

New_DataFrame['Muffin'] = New_DataFrame['FOOD'].map(Muffinfct)

def Cookiefct(x):
    if x == 'Cookie':
        return 1
    else:
        return 0

New_DataFrame['Cookie'] = New_DataFrame['FOOD'].map(Cookiefct)

def Nothingfct(x):
    if x not in ('Sandwich', 'Pie', 'Muffin', 'Cookie'):
        return 1
    else:
        return 0

New_DataFrame['Nothing'] = New_DataFrame['FOOD'].map(Nothingfct)

Count_Food = New_DataFrame[['YEAR','Sandwich','Pie', 'Cookie', 'Muffin', 'Nothing']].groupby('YEAR', as_index=False).sum()

Count_Food.plot(x='YEAR', y=['Sandwich', 'Pie', 'Cookie', 'Muffin', 'Nothing'], kind="bar", title=" Number of foods sold per year")
plt.savefig(path2 + "New_Plot_Count_Food") #do the two commands at th same time

#bar plot for drinks

def Frappfct(x):
    if x == 'Frappucino':
        return 1
    else:
        return 0

New_DataFrame['Frappucino'] = New_DataFrame['DRINK'].map(Frappfct)

def Sodafct(x):
    if x == 'Soda':
        return 1
    else:
        return 0

New_DataFrame['Soda'] = New_DataFrame['DRINK'].map(Sodafct)

def Coffeefct(x):
    if x == 'Coffee':
        return 1
    else:
        return 0

New_DataFrame['Coffee'] = New_DataFrame['DRINK'].map(Coffeefct)

def Teafct(x):
    if x == 'Tea':
        return 1
    else:
        return 0

New_DataFrame['Tea'] = New_DataFrame['DRINK'].map(Teafct)

def Waterfct(x):
    if x == 'Water':
        return 1
    else:
        return 0

New_DataFrame['Water'] = New_DataFrame['DRINK'].map(Waterfct)

def Milkfct(x):
    if x == 'Milkshake':
        return 1
    else:
        return 0

New_DataFrame['Milkshake'] = New_DataFrame['DRINK'].map(Milkfct)

Count_Drinks = New_DataFrame[['YEAR','Frappucino','Tea', 'Water', 'Coffee','Soda', 'Milkshake']].groupby('YEAR', as_index=False).sum()

Count_Drinks.plot(x='YEAR', y=['Frappucino', 'Tea', 'Water', 'Coffee','Soda', 'Milkshake'], kind="bar", title='Number of drinks sold per year')
plt.savefig(path2 + "New_Plot_Count_Drinks") #do the two commands at th same time

# Bar plots for income

# Average income per hour

New_DataFrame['Hour'] = New_DataFrame['TIME'].str[11:13]
Income_Hour = New_DataFrame[['Hour', 'TOTAL']].groupby('Hour', as_index=False).sum()
Income_Hour['TOTAL']=Income_Hour['TOTAL']/1825
Income_Hour.plot(x='Hour', y='TOTAL', kind='bar', title='Average income per hour')
plt.savefig(path2 + 'Income_Hour') #do the two commands at the same time

# Average income per month

New_DataFrame['Month'] = New_DataFrame['TIME'].str[5:7]
Income_Month = New_DataFrame[['Month', 'TOTAL']].groupby('Month', as_index=False).sum()
Income_Month['TOTAL']= Income_Month['TOTAL']/5
Income_Month.plot(x='Month', y='TOTAL', kind='bar', title='Average income per month')
plt.savefig(path2 + 'Income_Month') #do the two commands at the same time





# begin of part 4 (Show some buying histories of returning customers for your simulations)

CustomerReturning_List[85].GetHistory(New_DataFrame) #You can choose the number of the customer you want




