#Part 1

#First of all, make sure to read our comments before submitting selection.

#What drinks and food are sold ? How many unique customers ?

import pandas as pd
path = "./Data/"      #With this path, you have to use the submitt selection (alt maj E), in the whole document

Coffeebar = pd.read_csv(path+"Coffeebar_2013-2017.csv", sep=";")

Coffeebar['FOOD2']=Coffeebar['FOOD'].fillna('nothing')
print(Coffeebar["DRINKS"].unique()) #list of drinks
print(Coffeebar["FOOD2"].unique()) #list of food

Customer_count = Coffeebar['CUSTOMER'].value_counts()
Customer_count[Customer_count<2]
print(len(Customer_count[Customer_count<2])) #number of unique customers(one time customers)

#a bar plot of the total amount of sold foods (plot1) and drinks (plot2) over the five years

import matplotlib.pyplot as plt
path2 = "./Results/"
Coffeebar['YEAR'] = Coffeebar['TIME'].str[0:4]

#Bar plot for food
def Sandfct(x):
    if x == 'sandwich':
        return 1
    else:
        return 0

Coffeebar['Sandwich'] = Coffeebar['FOOD'].map(Sandfct)

def Piefct(x):
    if x == 'pie':
        return 1
    else:
        return 0

Coffeebar['Pie'] = Coffeebar['FOOD'].map(Piefct)

def Muffinfct(x):
    if x == 'muffin':
        return 1
    else:
        return 0

Coffeebar['Muffin'] = Coffeebar['FOOD'].map(Muffinfct)

def Cookiefct(x):
    if x == 'cookie':
        return 1
    else:
        return 0

Coffeebar['Cookie'] = Coffeebar['FOOD'].map(Cookiefct)

def Nothingfct(x):
    if x not in ('sandwich', 'pie', 'muffin', 'cookie'):
        return 1
    else:
        return 0

Coffeebar['Nothing'] = Coffeebar['FOOD'].map(Nothingfct)

Count_Food = Coffeebar[['YEAR','Sandwich','Pie', 'Cookie', 'Muffin', 'Nothing']].groupby('YEAR', as_index=False).sum()

Count_Food.plot(x='YEAR', y=['Sandwich', 'Pie', 'Cookie', 'Muffin', 'Nothing'], kind="bar", title='Number of foods sold per year')
plt.savefig(path2 + "Plot_Count_Food") #do the two commands at th same time and then close the image to save it

#bar plot for drinks

def Frappfct(x):
    if x == 'frappucino':
        return 1
    else:
        return 0

Coffeebar['Frappucino'] = Coffeebar['DRINKS'].map(Frappfct)

def Sodafct(x):
    if x == 'soda':
        return 1
    else:
        return 0

Coffeebar['Soda'] = Coffeebar['DRINKS'].map(Sodafct)

def Coffeefct(x):
    if x == 'coffee':
        return 1
    else:
        return 0

Coffeebar['Coffee'] = Coffeebar['DRINKS'].map(Coffeefct)

def Teafct(x):
    if x == 'tea':
        return 1
    else:
        return 0

Coffeebar['Tea'] = Coffeebar['DRINKS'].map(Teafct)

def Waterfct(x):
    if x == 'water':
        return 1
    else:
        return 0

Coffeebar['Water'] = Coffeebar['DRINKS'].map(Waterfct)

def Milkfct(x):
    if x =='milkshake':
        return 1
    else: return 0

Coffeebar['Milkshake'] = Coffeebar['DRINKS'].map(Milkfct)

Count_Drinks = Coffeebar[['YEAR','Frappucino','Tea', 'Water', 'Coffee','Soda', 'Milkshake']].groupby('YEAR', as_index=False).sum()

Count_Drinks.plot(x='YEAR', y=['Frappucino', 'Tea', 'Water', 'Coffee','Soda', 'Milkshake'], kind="bar", title='Number of drinks sold per year')
plt.savefig(path2 + "Plot_Count_Drinks") #do the two commands at th same time

#Determine the average that a customer buys a certain food or drink at any given time

#Food
Coffeebar['HOUR'] = Coffeebar['TIME'].str[11:16]

#We create a dataframe of probabilities
Coffeebar['Nothing'] = Coffeebar['Nothing'] / 1825 * 100
Coffeebar['Sandwich'] = Coffeebar['Sandwich'] / 1825 * 100
Coffeebar['Pie'] = Coffeebar['Pie'] / 1825 * 100
Coffeebar['Muffin'] = Coffeebar['Muffin'] / 1825 * 100
Coffeebar['Cookie'] = Coffeebar['Cookie'] / 1825 * 100

Prob_Food = Coffeebar[['HOUR','Sandwich','Pie', 'Cookie', 'Muffin', 'Nothing']].groupby('HOUR', as_index=False).sum()
Prob_Food2 = Prob_Food.set_index("HOUR")

#we use the first one to make a new dataframe with cumulative probabilities
Prob_Food3 = Prob_Food[['HOUR']]
Prob_Food3['Sandwich']=Prob_Food['Sandwich']
Prob_Food3['Pie']= Prob_Food['Sandwich'] + Prob_Food['Pie']
Prob_Food3['Cookie']= Prob_Food['Sandwich'] + Prob_Food['Pie'] + Prob_Food['Cookie']
Prob_Food3['Muffin']= Prob_Food['Sandwich'] + Prob_Food['Pie'] + Prob_Food['Cookie'] + Prob_Food['Muffin']
Prob_Food3['Nothing']= Prob_Food['Sandwich'] + Prob_Food['Pie'] + Prob_Food['Cookie'] + Prob_Food['Muffin'] + Prob_Food['Nothing']
Prob_Food3 = Prob_Food3.set_index("HOUR")

Prob_Food3.to_csv(path2 + 'ProbFoodPart3', sep=',')

 #Drinks
Coffeebar['Coffee'] = Coffeebar['Coffee'] / 1825 * 100
Coffeebar['Tea'] = Coffeebar['Tea'] / 1825 * 100
Coffeebar['Soda'] = Coffeebar['Soda'] / 1825 * 100
Coffeebar['Water'] = Coffeebar['Water'] / 1825 * 100
Coffeebar['Frappucino'] = Coffeebar['Frappucino'] / 1825 * 100
Coffeebar['Milkshake'] = Coffeebar['Milkshake'] / 1825 * 100

Prob_Drink = Coffeebar [['HOUR','Frappucino','Soda','Coffee','Tea','Water','Milkshake']].groupby('HOUR',as_index=False).sum()

Prob_Drink2=Prob_Drink.set_index("HOUR")

Prob_Drink3 = Prob_Drink[['HOUR']]
Prob_Drink3['Frappucino']=Prob_Drink['Frappucino']
Prob_Drink3['Soda']= Prob_Drink['Frappucino'] + Prob_Drink['Soda']
Prob_Drink3['Coffee']= Prob_Drink['Frappucino'] + Prob_Drink['Soda'] + Prob_Drink['Coffee']
Prob_Drink3['Tea']= Prob_Drink['Frappucino'] + Prob_Drink['Soda'] + Prob_Drink['Coffee'] + Prob_Drink['Tea']
Prob_Drink3['Water']= Prob_Drink['Frappucino'] + Prob_Drink['Soda'] + Prob_Drink['Coffee'] + Prob_Drink['Tea'] + Prob_Drink['Water']
Prob_Drink3['Milkshake']= Prob_Drink['Frappucino'] + Prob_Drink['Soda'] + Prob_Drink['Coffee'] + Prob_Drink['Tea'] + Prob_Drink['Water'] + Prob_Drink['Milkshake']
Prob_Drink3 = Prob_Drink3.set_index("HOUR")

Prob_Drink3.to_csv(path2 + 'ProbDrinkPart3', sep=',')

#Function which displays the probability when you give the hour you want
def FoodAndDrink_average(x):
    message = "On average, the probability of a customer at " + x + \
              " buying frappucino, soda, coffee, tea, water, milkshake is " \
              + str(Prob_Drink2.loc[x,'Frappucino']) +'%, ' + str(Prob_Drink2.loc[x,'Soda'])+'%, ' + str(Prob_Drink2.loc[x,'Coffee']) \
              +'%, ' + str(Prob_Drink2.loc[x,'Tea']) +'%, ' + str(Prob_Drink2.loc[x,'Water']) +'%, ' + str(Prob_Drink2.loc[x,'Milkshake'])+\
              '%, ' + "and for food "+ str(Prob_Food2.loc[x,'Sandwich']) +'% sandwich, ' + str(Prob_Food2.loc[x,'Pie'])+'% pie, ' + str(Prob_Food2.loc[x,'Muffin'])\
              +'% muffin, ' + str(Prob_Food2.loc[x,'Cookie']) +'% cookie, ' + str(Prob_Food2.loc[x,'Nothing']) +'% nothing'
    return message

FoodAndDrink_average('11:30') #put the hour you want as string


